package database

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Add(t *testing.T) {
	tests := map[string]struct {
		db     *MemoryDB
		record MemoryRecord

		assertErr assert.ErrorAssertionFunc
		errStr    string
	}{
		"empty record": {
			db:        NewMemory(),
			assertErr: assert.Error,
		},
		"empty key": {
			db:        NewMemory(),
			record:    MemoryRecord{Value: "myValue"},
			assertErr: assert.Error,
		},
		"empty value": {
			db:        NewMemory(),
			record:    MemoryRecord{Key: "myKey"},
			assertErr: assert.Error,
		},
		"valid record": {
			db:        NewMemory(),
			record:    MemoryRecord{Key: "myKey", Value: "myValue"},
			assertErr: assert.NoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.db.Add(tt.record)
			tt.assertErr(t, err)
		})
	}
}

func Test_Get(t *testing.T) {
	tests := map[string]struct {
		db   *MemoryDB
		key  string
		want MemoryRecord
		ok   bool
	}{
		"no key specified": {
			db: NewMemory(),
		},
		"key not existing": {
			db:  NewMemory(),
			key: "myKey",
		},
		"existing value": {
			db:   &MemoryDB{db: map[string]string{"myKey": "myValue"}},
			key:  "myKey",
			want: MemoryRecord{Key: "myKey", Value: "myValue"},
			ok:   true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, ok := tt.db.Get(tt.key)

			assert.Equal(t, ok, tt.ok)
			if ok {
				assert.Equal(t, got, tt.want)
			}
		})
	}
}
