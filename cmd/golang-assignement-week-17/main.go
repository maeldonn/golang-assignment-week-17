package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/maeldonn/golang-assignment-week-17/config"
	"gitlab.com/maeldonn/golang-assignment-week-17/database"
	"gitlab.com/maeldonn/golang-assignment-week-17/handlers"
)

func main() {
	// get app configuration
	cfg, err := config.ParseConfig()
	if err != nil {
		log.Fatalf("cannot parse configuration: %v", err)
	}

	// connect to mongo
	client, err := database.Connect(cfg.MongoURI)
	if err != nil {
		log.Fatalf("cannot connect to database: %v", err)
	}
	defer client.Disconnect(context.Background())

	handler := handlers.New(cfg, client.Database(cfg.Database))

	http.HandleFunc("POST /records", Logger(handler.GetMongoRecords))
	http.HandleFunc("GET /in-memory", Logger(handler.GetInMemory))
	http.HandleFunc("POST /in-memory", Logger(handler.PostInMemory))

	port := fmt.Sprintf(":%d", cfg.Port)

	log.Printf("Started server on port %s", port)
	if err := http.ListenAndServe(port, nil); !errors.Is(err, http.ErrServerClosed) {
		log.Fatalf("HTTP server error: %v", err)
	}
}

// Logger is a logging middleware
func Logger(handler http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		handler.ServeHTTP(w, r)
		log.Printf("%s %s %v", r.Method, r.URL.Path, time.Since(start))
	})
}
