package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"gitlab.com/maeldonn/golang-assignment-week-17/config"
	"gitlab.com/maeldonn/golang-assignment-week-17/database"
	"gitlab.com/maeldonn/golang-assignment-week-17/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Handler is the main handler
type Handler struct {
	MongoDB  *mongo.Database
	MemoryDB *database.MemoryDB
	Config   *config.Config
}

// New create a Handler
func New(cfg *config.Config, mongoDB *mongo.Database) *Handler {
	return &Handler{
		MongoDB:  mongoDB,
		MemoryDB: database.NewMemory(),
		Config:   cfg,
	}
}

// GetMongoRecords find mongo records with filters
func (h Handler) GetMongoRecords(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	var filters types.RecordRequest
	err := json.NewDecoder(r.Body).Decode(&filters)
	if err != nil && r.ContentLength != 0 {
		slog.Error("error while decoding filters", slog.String("error", err.Error()))
		writeJSON(w, types.ErrReponse(err), http.StatusBadRequest)
		return
	}

	pipeline := mongo.Pipeline{
		{{
			Key:   "$unwind",
			Value: "$counts",
		}},
		{{
			Key: "$group",
			Value: bson.M{
				"_id":        "$_id",
				"key":        bson.M{"$first": "$key"},
				"createdAt":  bson.M{"$first": "$createdAt"},
				"totalCount": bson.M{"$sum": "$counts"},
			},
		}},
	}

	matches, err := filters.Matches()
	if err != nil {
		slog.Error("error while generating matches", slog.String("error", err.Error()))
		writeJSON(w, types.ErrReponse(err), http.StatusBadRequest)
		return
	}
	pipeline = append(pipeline, matches...)

	cursor, err := h.MongoDB.Collection(h.Config.Collection).Aggregate(ctx, pipeline)
	if err != nil {
		slog.Error("error while aggregating collection", slog.String("error", err.Error()))
		writeJSON(w, types.ErrReponse(err), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(ctx)

	var records []types.Record
	if err := cursor.All(ctx, &records); err != nil {
		slog.Error("error while decoding collection", slog.String("error", err.Error()))
		writeJSON(w, types.ErrReponse(err), http.StatusInternalServerError)
		return
	}

	if err := cursor.Err(); err != nil {
		slog.Error("error while reading data", slog.String("error", err.Error()))
		writeJSON(w, types.ErrReponse(err), http.StatusInternalServerError)
		return
	}

	writeJSON(w, types.OKReponse(records), http.StatusOK)
}

// GetInMemory get a record in a memory store
func (h Handler) GetInMemory(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get("key")
	if key == "" {
		err := fmt.Errorf("key is empty")
		slog.Error("cannot get value in memory db", slog.String("error", err.Error()))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if record, ok := h.MemoryDB.Get(key); ok {
		writeJSON(w, record, http.StatusOK)
		return
	}

	err := fmt.Errorf("key not existing")
	slog.Error("cannot get value in memory store", slog.String("error", err.Error()))
	http.Error(w, err.Error(), http.StatusNotFound)
}

// PostInMemory save a record in a memory store
func (h Handler) PostInMemory(w http.ResponseWriter, r *http.Request) {
	var record database.MemoryRecord
	if err := json.NewDecoder(r.Body).Decode(&record); err != nil {
		slog.Error("Failed to get request body", slog.String("error", err.Error()))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer r.Body.Close()

	if err := h.MemoryDB.Add(record); err != nil {
		slog.Error("Failed to add a new record", slog.String("error", err.Error()))
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeJSON(w, record, http.StatusCreated)
}

// writeJSON Write a JSON with an associated status
func writeJSON(w http.ResponseWriter, v interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(v)
}
