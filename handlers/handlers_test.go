package handlers

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/maeldonn/golang-assignment-week-17/database"
)

func Test_GetInMemory(t *testing.T) {
	nonEmptyDB := database.NewMemory()
	_ = nonEmptyDB.Add(database.MemoryRecord{Key: "myKey", Value: "myValue"})

	tests := map[string]struct {
		handler   *Handler
		urlValues map[string]string
		wantBody  string
		wantCode  int
	}{
		"empty key": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			wantBody: "key is empty\n",
			wantCode: http.StatusBadRequest,
		},
		"not existing key": {
			handler:   &Handler{MemoryDB: database.NewMemory()},
			urlValues: map[string]string{"key": "myKey"},
			wantBody:  "key not existing\n",
			wantCode:  http.StatusNotFound,
		},
		"existing key": {
			handler:   &Handler{MemoryDB: nonEmptyDB},
			urlValues: map[string]string{"key": "myKey"},
			wantBody:  "{\"key\":\"myKey\",\"value\":\"myValue\"}\n",
			wantCode:  http.StatusOK,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			rr := httptest.NewRecorder()

			req, err := http.NewRequest(http.MethodGet, "in-memory", nil)
			if err != nil {
				t.Error(err)
			}

			q := req.URL.Query()
			for k, v := range tt.urlValues {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()

			tt.handler.GetInMemory(rr, req)

			assert.Equal(t, rr.Result().StatusCode, tt.wantCode)

			b, err := io.ReadAll(rr.Result().Body)
			if err != nil {
				t.Error(err)
			}
			defer rr.Result().Body.Close()

			assert.Equal(t, string(b), tt.wantBody)
		})
	}
}

func Test_GetPostInMemory(t *testing.T) {
	tests := map[string]struct {
		handler  *Handler
		body     string
		wantBody string
		wantCode int
	}{
		"no body": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			wantBody: "EOF\n",
			wantCode: http.StatusInternalServerError,
		},
		"bad json body": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			body:     "{\"key\": \"myKey\", \"value\":\"myValue\"",
			wantBody: "unexpected EOF\n",
			wantCode: http.StatusInternalServerError,
		},
		"invalid record no key": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			body:     "{\"value\":\"myValue\"}",
			wantBody: "invalid record\n",
			wantCode: http.StatusBadRequest,
		},
		"invalid record no value": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			body:     "{\"key\": \"myKey\"}",
			wantBody: "invalid record\n",
			wantCode: http.StatusBadRequest,
		},
		"valid record": {
			handler:  &Handler{MemoryDB: database.NewMemory()},
			body:     "{\"key\": \"myKey\", \"value\":\"myValue\"}",
			wantBody: "{\"key\":\"myKey\",\"value\":\"myValue\"}\n",
			wantCode: http.StatusCreated,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			rr := httptest.NewRecorder()

			req, err := http.NewRequest(http.MethodPost, "in-memory", strings.NewReader(tt.body))
			if err != nil {
				t.Error(err)
			}

			tt.handler.PostInMemory(rr, req)

			assert.Equal(t, rr.Result().StatusCode, tt.wantCode)

			b, err := io.ReadAll(rr.Result().Body)
			if err != nil {
				t.Error(err)
			}
			defer rr.Result().Body.Close()

			assert.Equal(t, string(b), tt.wantBody)
		})
	}
}
