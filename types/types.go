package types

import (
	"encoding/json"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type RecordRequest struct {
	StartDate *string `json:"startDate"`
	EndDate   *string `json:"endDate"`
	MinCount  *int    `json:"minCount"`
	MaxCount  *int    `json:"maxCount"`
}

func (f RecordRequest) Matches() (mongo.Pipeline, error) {
	var pipeline mongo.Pipeline

	if f.StartDate != nil {
		startDate, err := time.Parse(time.DateOnly, *f.StartDate)
		if err != nil {
			return pipeline, fmt.Errorf("invalid date format: %w", err)
		}

		pipe := mongo.Pipeline{
			{{
				Key: "$match",
				Value: bson.M{
					"createdAt": bson.M{"$gte": startDate},
				},
			}},
		}
		pipeline = append(pipeline, pipe...)
	}

	if f.EndDate != nil {
		endDate, err := time.Parse(time.DateOnly, *f.EndDate)
		if err != nil {
			return pipeline, fmt.Errorf("invalid date format: %w", err)
		}

		pipe := mongo.Pipeline{
			{{
				Key: "$match",
				Value: bson.M{
					"createdAt": bson.M{"$lte": endDate},
				},
			}},
		}
		pipeline = append(pipeline, pipe...)
	}

	if f.MinCount != nil {
		pipe := mongo.Pipeline{
			{{
				Key: "$match",
				Value: bson.M{
					"totalCount": bson.M{"$gte": *f.MinCount},
				},
			}},
		}
		pipeline = append(pipeline, pipe...)
	}

	if f.MaxCount != nil {
		pipe := mongo.Pipeline{
			{{
				Key: "$match",
				Value: bson.M{
					"totalCount": bson.M{"$lte": *f.MaxCount},
				},
			}},
		}
		pipeline = append(pipeline, pipe...)
	}

	return pipeline, nil
}

type Record struct {
	ID         string    `bson:"_id" json:"-"`
	Key        string    `bson:"key" json:"key"`
	CreatedAt  time.Time `bson:"createdAt" json:"createdAt"`
	TotalCount int       `bson:"totalCount" json:"totalCount"`
}

func (r Record) MarshalJSON() ([]byte, error) {
	parsed := struct {
		Key        string `json:"key"`
		CreatedAt  string `json:"createdAt"`
		TotalCount int    `json:"totalCount"`
	}{
		Key:        r.Key,
		CreatedAt:  r.CreatedAt.Format(time.DateOnly),
		TotalCount: r.TotalCount,
	}
	return json.Marshal(parsed)
}

type RecordReponse struct {
	Code    int      `json:"code"`
	Message string   `json:"msg"`
	Records []Record `json:"records"`
}

func ErrReponse(err error) RecordReponse {
	return RecordReponse{Code: 1, Message: err.Error(), Records: []Record{}}
}

func OKReponse(records []Record) RecordReponse {
	if records == nil {
		records = []Record{}
	}

	return RecordReponse{
		Code:    0,
		Message: "Success",
		Records: records,
	}
}
