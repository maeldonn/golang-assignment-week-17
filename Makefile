export MONGO_URI=
export DATABASE=getircase-study
export COLLECTION=records
export PORT=8081

all: run

build:
	go build -o bin/ggchallenge cmd/ggchallenge/main.go

run: build
	./bin/ggchallenge

dev:
	go run cmd/ggchallenge/main.go

test:
	go test ./...

lint:
	golangci-lint run

gojq:
	command -v gojq > /dev/null 2>&1 || go install github.com/itchyny/gojq/cmd/gojq@latest

records: gojq 
	echo "Getting record without filters"
	curl -X POST "localhost:8081/records" -s | gojq
	sleep 1
	echo "Getting record with startDate=2017-01-28"
	curl -X POST "localhost:8081/records" -d '{"startDate": "2017-01-28"}' -s | gojq
	sleep 1
	echo "Getting record with endDate=2017-01-28"
	curl -X POST "localhost:8081/records" -d '{"endDate": "2017-01-28"}' -s | gojq
	sleep 1
	echo "Getting record with minCount=3000"
	curl -X POST "localhost:8081/records" -d '{"minCount": 3000}' -s | gojq
	sleep 1
	echo "Getting record with maxCount=2800"
	curl -X POST "localhost:8081/records" -d '{"maxCount": 2800}' -s | gojq

memory: gojq
	echo "Getting record for key 'Dog'..."
	curl "localhost:8081/in-memory?key=Dog" -s
	sleep 1
	echo "Setting record for key 'Dog'..."
	curl -X POST "localhost:8081/in-memory" -d '{ "key": "Dog", "value": "Corgi" }' -s | gojq
	sleep 1
	echo "Getting record for key 'Dog'..."
	curl "localhost:8081/in-memory?key=Dog" -s | gojq

.SILENT: build run dev records gojq memory
.PHONY: build run dev records gojq memory
