package config

import (
	"fmt"
	"os"
	"strconv"
)

// Config is the application configuration
type Config struct {
	MongoURI   string
	Database   string
	Collection string
	Port       int
}

// ParseConfig parse config from env variables
func ParseConfig() (*Config, error) {
	cfg := Config{
		Database:   "getircase-study",
		Collection: "records",
		Port:       8081,
	}

	uri := os.Getenv("MONGO_URI")
	if uri == "" {
		return nil, fmt.Errorf("mongo uri no specfied")
	}
	cfg.MongoURI = uri

	if db, ok := os.LookupEnv("DATABASE"); ok {
		cfg.Database = db
	}

	if c, ok := os.LookupEnv("COLLECTION"); ok {
		cfg.Collection = c
	}

	if p, ok := os.LookupEnv("PORT"); ok {
		port, err := strconv.Atoi(p)
		if err != nil {
			return nil, fmt.Errorf("cannot parse port: %w", err)
		}
		cfg.Port = port
	}

	return &cfg, nil
}
