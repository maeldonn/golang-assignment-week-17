# Golang Assignment #week17

_(Create a RESTFUL API with two endpoints)_

## Endpoints

### Mongo

- POST /records

### In-memory database

- GET /in-memory
- POST /in-memory

## How to test

First launch the API:
```bash
$ make run
```

Then:

To test records endpoints:
```bash
$ make records
```

To test in memory endpoints:
```bash
$ make memory
```

## How to configure

You can configure the API with ENV variables:

required:
- MONGO_URI

mandatory:
- DATABASE
- COLLECTION
- PORT

## Prerequisites

- go1.22
